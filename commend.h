#ifndef COMMEND_H
#define COMMEND_H

#include <iostream>
#include <cstdlib>
#include <windows.h>
#include <sys/stat.h>
using namespace std;
void process(string a)
{
    system(a.data());
}
bool isFileExists_stat(string& name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}
#endif // COMMEND_H
