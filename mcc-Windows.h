#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QTextEdit>
#include <QFileDialog>
#include <io.h>
#include <dirent.h>
#include <string.h>
#include <cstdlib>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class mcc_Windows : public QMainWindow
{
    Q_OBJECT

public:
    mcc_Windows(QWidget *parent = nullptr);
    ~mcc_Windows();
    QWidget *widget;
    QLineEdit *line;
    QPushButton *browser;
    QPushButton *sure;
    QTextEdit *fail;
    QRadioButton *r1;
    QRadioButton *r2;
    void init();

private:
    Ui::MainWindow *ui;

private slots:
    void brow();
    void compile();
};
#endif // MAINWINDOW_H
