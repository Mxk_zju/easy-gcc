#include "mcc-Windows.h"
#include "ui_mainwindow.h"
#include "commend.h"
mcc_Windows::mcc_Windows(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();

    connect(browser,SIGNAL(clicked()),this,SLOT(brow()));
    connect(sure,SIGNAL(clicked()),this,SLOT(compile()));
}

mcc_Windows::~mcc_Windows()
{
    delete ui;
}

void mcc_Windows::brow()
{
    QString current = line->text();
    QByteArray temp = current.toLatin1();
    QString directory;
    if(access(temp.data(),0))
        directory = QFileDialog::getExistingDirectory(this,tr("选择文件夹"),"/home");
    else
        directory = QFileDialog::getExistingDirectory(this,tr("选择文件夹"),current);
    if(directory.size())
        line->setText(directory);
}

void mcc_Windows::compile()
{
    QString current = line->text();
    QByteArray temp = current.toLatin1();
    QString directory;
    std::string commend, temp1, temp2=" ",temp3="/",exe;
    if(access(temp.data(),0))
    {
        QMessageBox:: StandardButton notice;
        notice = QMessageBox::critical(this,tr("错误"),"文件夹不存在",QMessageBox::Ok);
        return ;
    }
    if(!r1->isChecked()&&!r2->isChecked())
    {
        QMessageBox:: StandardButton notice;
        notice = QMessageBox::critical(this,tr("错误"),"请选择c/c++",QMessageBox::Ok);
        return ;
    }
    if(r1->isChecked())
        commend = "gcc -o ";
    else commend = "g++ -o ";
    commend+=(temp.data());
    commend+="/a.exe";
    exe = temp.data();
    exe+="/a.exe";
    DIR *base = opendir(temp.data());
    struct dirent* files;
    while((files=readdir(base))!=NULL)
    {
        temp1 = files->d_name;
        if(temp1.size()>=3&&temp1.substr(temp1.size()-2)==".h")
            commend += (temp2+temp.data()+temp3+temp1);
        else if(temp1.size()>=3&&temp1.substr(temp1.size()-2)==".c"&&r1->isChecked())
            commend += (temp2+temp.data()+temp3+temp1);
        else if(temp1.size()>=5&&temp1.substr(temp1.size()-4)==".cpp"&&r2->isChecked())
            commend += (temp2+temp.data()+temp3+temp1);
    }
    commend += " 2>output.txt";
    fail->setText(commend.data());
    process(commend);
    QFile output("output.txt");
    if(!output.open(QIODevice::ReadOnly|QIODevice::Text))
        return ;
    QTextStream output_stream(&output);
    fail->clear();
    while(!output_stream.atEnd())
    {
        QString l = output_stream.readLine();
        fail->append(l);
    }
    output.close();
    commend = "del output.txt";
    process(commend);
    if(!isFileExists_stat(exe))
    {
        QMessageBox:: StandardButton notice;
        notice = QMessageBox::critical(this,tr("错误"),"编译未通过",QMessageBox::Ok);
        return ;
    }
    process(exe);
}

void mcc_Windows::init()
{
    this->setWindowTitle("mcc-Windows");
    widget = new QWidget(this);
    line = new QLineEdit(this);
    browser = new QPushButton(this);
    browser->setText("浏览");
    sure = new QPushButton(this);
    sure->setText("确定");
    QHBoxLayout *h1 = new QHBoxLayout;
    h1->addWidget(line);
    h1->addWidget(browser);
    h1->addWidget(sure);
    r1 = new QRadioButton(this);
    r1->setText("c");
    r2 = new QRadioButton(this);
    r2->setText("c++");
    QHBoxLayout *h2 = new QHBoxLayout;
    h2->addWidget(r1);
    h2->addWidget(r2);
    QVBoxLayout *V = new QVBoxLayout;
    V->addLayout(h2);
    V->addLayout(h1);
    fail = new QTextEdit(this);
    V->addWidget(fail);
    widget->setLayout(V);
    setCentralWidget(widget);
}
